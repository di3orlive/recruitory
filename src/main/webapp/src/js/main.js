// stick header
$(window).on("scroll touchmove", function () {
    $('.rec-header-nav').toggleClass('tiny', $(document).scrollTop() > 0);
});
// scroll to anchor on click
$('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

        $(this).parent().children('a').removeClass('active');
        $(this).addClass('active');

        if (target.length) {
            $('html,body').stop().animate({
                scrollTop: target.offset().top - 30
            }, 1000);
            return false;
        }
    }
});
// send form
var mailForm = $("#mail-form");
mailForm.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
mailForm.submit(function (ev) {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        dataType: "json",
        url: "rest/contact/mail",
        data: JSON.stringify(mailForm.serializeObject()),
        success: successHandler
    });
    ev.preventDefault();
});
var inTouchForm = $("#in-touch-form");
inTouchForm.submit(function (ev) {
    $.ajax({
        type: "POST",
        contentType: "text/plain",
        dataType: "text",
        url: "rest/contact/intouch",
        data: $("#touch-mail").val(),
        success: successHandler
    });
    ev.preventDefault();
});
var successHandler = function () {
    alert("Thank you! Your request was submitted, we will come back to you.");
};