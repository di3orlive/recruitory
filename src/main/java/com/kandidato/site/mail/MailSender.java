package com.kandidato.site.mail;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * This class s capable of sending e-mails.
 * <p/>
 * Created by andriy on 1/3/15.
 */
public class MailSender {

    private static final String RECRUTORY_GOOGLE_MAIL = "recrutory@gmail.com";
    private static final String RECRUTORY_INFO_MAIL = "info@recrutory.com";


    public static void send(String senderName, String senderMail, String subject, String messageText) {
        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);
        try {
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(RECRUTORY_GOOGLE_MAIL, "Recrutory"));
            msg.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(RECRUTORY_INFO_MAIL, "Recrutory"));
            msg.setSubject(subject);
            msg.setText(String.format("%s <%s> : %s", senderName, senderMail, messageText));
            Transport.send(msg);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
