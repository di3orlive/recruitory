package com.kandidato.site.service;

import com.kandidato.site.mail.MailSender;
import com.kandidato.site.model.MailRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

@Path("contact")
public class ContactService {

    @POST
    @Path("/mail")
    @Consumes({MediaType.APPLICATION_JSON})
    public void requestContact(MailRequest request) {
        MailSender.send(request.getSenderName(), request.getSenderEmail(), request.getSubject(), request.getMessageText());
    }

    @POST
    @Path("/intouch")
    @Consumes({MediaType.TEXT_PLAIN})
    public void addToStayInTouchList(String email) {
        MailSender.send(email, email, "Stay in touch request",
                "This message was sent, because someone submitted an e-mail via the stay-in-touch field.");
    }

}
