package com.kandidato.site.config;

import com.kandidato.site.service.ContactService;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by andriy on 1/4/15.
 */
public class RestApplication extends javax.ws.rs.core.Application {

    private final Set<Object> singletons = new HashSet<Object>();

    public RestApplication() {
        this.singletons.add(new ContactService());
    }

    @Override
    public Set<Object> getSingletons() {
        return this.singletons;
    }
}
