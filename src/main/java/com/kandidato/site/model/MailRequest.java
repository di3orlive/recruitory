package com.kandidato.site.model;

/**
 * Created by andriy on 1/4/15.
 */
public class MailRequest {

    private String senderName;
    private String subject;
    private String senderEmail;
    private String messageText;

    /**
     * This constructor should only be used for json de-serialization.
     */
    public MailRequest() {
    }

    public MailRequest(String senderName, String subject, String senderEmail, String messageText) {
        this.senderName = senderName;
        this.subject = subject;
        this.senderEmail = senderEmail;
        this.messageText = messageText;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getSubject() {
        return subject;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public String getMessageText() {
        return messageText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MailRequest)) return false;

        MailRequest that = (MailRequest) o;

        if (!messageText.equals(that.messageText)) return false;
        if (!senderEmail.equals(that.senderEmail)) return false;
        if (!senderName.equals(that.senderName)) return false;
        if (!subject.equals(that.subject)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = senderName.hashCode();
        result = 31 * result + subject.hashCode();
        result = 31 * result + senderEmail.hashCode();
        result = 31 * result + messageText.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "MailRequest{" +
                "senderName='" + senderName + '\'' +
                ", subject='" + subject + '\'' +
                ", senderEmail='" + senderEmail + '\'' +
                ", messageText='" + messageText + '\'' +
                '}';
    }
}
